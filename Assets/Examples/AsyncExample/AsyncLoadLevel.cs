﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UniRx;

public class AsyncLoadLevel : MonoBehaviour {

	// Use this for initialization
	void Start () {
		SceneManager.LoadSceneAsync ("HeavyScene")
			.AsAsyncOperationObservable ()
			.Do (x => {
				Debug.Log ("progress: " + x.progress);
			}).Subscribe (_ => {
				Debug.Log ("loaded");
			}).AddTo (this);
	}

}
