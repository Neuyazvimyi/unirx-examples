﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class HttpExample : MonoBehaviour {

	// Use this for initialization
	void Start () {
		var httpRequest = ObservableWWW.Get("http://api.duckduckgo.com/?q=habrahabr&format=json")
			.Subscribe(x => {
				Debug.Log ("res: "+x);
			}, ex => {
				Debug.Log ("error: "+ex);
			});
		
		// To cancel request
//		httpRequest.Dispose ();
	}

}
