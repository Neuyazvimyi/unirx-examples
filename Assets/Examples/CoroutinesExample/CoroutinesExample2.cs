﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class CoroutinesExample2 : MonoBehaviour {

	// Use this for initialization
	void Start () {
		// Starts coroutines one by one
		Observable.FromCoroutine (AsyncA)
			.SelectMany (AsyncB)
			.SelectMany (AsyncC)
			.Subscribe(_ => {
				Debug.Log ("end");
			}).AddTo (this);
	}
	
	IEnumerator AsyncA () {
		Debug.Log("a start");
		yield return new WaitForSeconds (1);
		Debug.Log("a end");
	}

	IEnumerator AsyncB () {
		Debug.Log("b start");
		yield return new WaitForFixedUpdate ();
		Debug.Log("b end");
	}

	IEnumerator AsyncC () {
		Debug.Log("c start");
		yield return new WaitForEndOfFrame ();
		Debug.Log("c end");
	}
}
