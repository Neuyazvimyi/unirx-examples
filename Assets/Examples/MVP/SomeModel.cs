﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class SomeModel {

	public ReactiveProperty<int> count { get; private set; }

	public SomeModel () {
		count = new ReactiveProperty<int> (0);
	}



}
