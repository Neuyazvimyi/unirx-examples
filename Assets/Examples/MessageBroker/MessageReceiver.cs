﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class MessageReceiver : MonoBehaviour {

	public CompositeDisposable disposables;

	// Use this for initialization
	void Start () {
		
	}
	
	void OnEnable () {
		disposables = new CompositeDisposable();

		MessageBroker.Default
			.Receive<MessageBase>()
			.Where(msg => msg.id == ServiceShareData.MSG_ATTACK)
			.Subscribe(msg => {
				string data = (string)msg.data;
				Debug.Log ("sender:"+msg.sender.name+" receiver:"+name+" data:"+data);
			}).AddTo (disposables);
	}

	void OnDisable () {
		if (disposables != null) {
			disposables.Dispose ();
		}
	}

}
