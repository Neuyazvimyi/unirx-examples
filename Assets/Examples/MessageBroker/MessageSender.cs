﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class MessageSender : MonoBehaviour {

	// Use this for initialization
	void Start () {
		MessageBroker.Default.Publish (MessageBase.Create (this, ServiceShareData.MSG_ATTACK, "attack!"));
	}

}
