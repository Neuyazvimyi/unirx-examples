﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class ThreadExample : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Observable.Start (() => {
			int n = 100000000;
			int res = Fibonacci(n);
			return res;
		}).ObserveOnMainThread ().Subscribe (xs => {
			Debug.Log ("res: "+xs);
		}).AddTo (this);
	}


	private int Fibonacci (int n) {
		int a = 0;
		int b = 1;

		for (int i = 0; i < n; i++) {
			int temp = a;
			a = b;
			b = temp + b;
		}

		return a;
	}

}
