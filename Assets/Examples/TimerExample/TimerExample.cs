﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class TimerExample : MonoBehaviour {

	public CompositeDisposable disposables;

	// Use this for initialization
	void Start () {
		Observable.Timer (System.TimeSpan.FromSeconds (3)).Subscribe (_ => {
			Debug.Log ("after 3 seconds");
		}).AddTo (disposables);

		Observable.Timer (System.TimeSpan.FromSeconds (1))
			.Repeat ()
			.Subscribe (_ => {
				Debug.Log ("every 1 second");
			}).AddTo (disposables);
	}

	void OnEnable () {
		disposables = new CompositeDisposable();
	}

	void OnDisable () {
		if (disposables != null) {
			disposables.Dispose ();
		}
	}

}
