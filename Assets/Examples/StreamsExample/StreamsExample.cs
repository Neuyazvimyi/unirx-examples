﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class StreamsExample : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Observable.EveryUpdate()
			.Where(_ => Input.anyKeyDown)
			.Select(_ => Input.inputString)
			.Subscribe (x => {
				OnKeyDown (x);
			}).AddTo (this);
	}

	private void OnKeyDown (string keyCode) {
		switch (keyCode) {
		case "w":
			Debug.Log ("keyCode: W");
			break;
		case "s":
			Debug.Log ("keyCode: S");
			break;
		case "a":
			Debug.Log ("keyCode: A");
			break;
		case "d":
			Debug.Log ("keyCode: D");
			break;
		default:
			Debug.Log ("keyCode: "+keyCode);
			break;
		}
	}

}
