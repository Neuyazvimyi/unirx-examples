﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SomeView : MonoBehaviour {

	public Text someText;
	public Button someButton;

	public void RenderCount (int count) {
		someText.text = count.ToString ();
	}

	public void AnimateButton () {
		someButton.transform.DOShakeScale (0.5F, 0.3F).OnComplete (() => {
			someButton.transform.localScale = Vector3.one;
		});
	}



}
