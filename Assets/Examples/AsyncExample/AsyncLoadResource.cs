﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class AsyncLoadResource : MonoBehaviour {

	// Use this for initialization
	void Start () {
		SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer> ();

		Resources.LoadAsync<Sprite> ("sprite")
			.AsAsyncOperationObservable ()
			.Subscribe (xs => {
				if (xs.asset != null) {
					Sprite sprite = xs.asset as Sprite;
					spriteRenderer.sprite = sprite;
				}
			}).AddTo (this);
	}

}
