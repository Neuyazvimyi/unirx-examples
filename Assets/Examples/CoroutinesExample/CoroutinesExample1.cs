﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class CoroutinesExample1 : MonoBehaviour {

	// Use this for initialization
	void Start () {
		// Starts all coroutines
		Observable.WhenAll (
			Observable.FromCoroutine (AsyncA),
			Observable.FromCoroutine (AsyncB),
			Observable.FromCoroutine (AsyncC)
		).Subscribe (_ => {
			Debug.Log ("end");
		}).AddTo (this);
	}

	IEnumerator AsyncA () {
		Debug.Log("a start");
		yield return new WaitForSeconds (1);
		Debug.Log("a end");
	}

	IEnumerator AsyncB () {
		Debug.Log("b start");
		yield return new WaitForFixedUpdate ();
		Debug.Log("b end");
	}

	IEnumerator AsyncC () {
		Debug.Log("c start");
		yield return new WaitForEndOfFrame ();
		Debug.Log("c end");
	}

}
