﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class SomePresenter : MonoBehaviour {

	public SomeView someView;
	public SomeModel someModel = new SomeModel ();

	// Use this for initialization
	void Start () {
		someModel.count.ObserveEveryValueChanged (x => x.Value).Subscribe (xs => {
			someView.RenderCount (xs);
		}).AddTo (this);

		someView.someButton.OnClickAsObservable ().Subscribe (_ => OnClick (someView.someButton.GetInstanceID ())).AddTo (this);

	}

	private void OnClick (int buttonId) {
		if (buttonId == someView.someButton.GetInstanceID ()) {
			someModel.count.Value++;
			someView.AnimateButton ();
		}
	}
	

}
